defmodule Assinante do
  @moduledoc """
  Modulo de assinante para cadastro de tipos de assinantes como `prepago` e `pospago`.

  A função mais utilizada é a função `cadastrar/4`
  """

  defstruct nome: nil, numero: nil, cpf: nil, plano: nil, chamadas: []

  @assinantes %{:prepago => "pre.txt", :pospago => "pos.txt"}

  @doc """
  Função para cadastrar assinante seja ele `prepago` e `pospago`

  ## Parametros da função

  - nome: parametro do nome do assinante
  - numero: numero unico e caso exista pode retornar um erro
  - cpf: parametro de assinante
  - plano: opcional e caso não seja informado será cadastrado a um `prepago`

  ## Informações Adicionais

  - caso o número já exista ele exibirá uma mensagem de erro

  ## Exemplo

      iex> Assinante.cadastrar("Fulano", "1234", "1234", :prepago)
      {:ok, "Assinante Fulano cadastrado com sucesso"}
  """
  def cadastrar(nome, numero, cpf, :prepago), do: cadastrar(nome, numero, cpf, %Prepago{})
  def cadastrar(nome, numero, cpf, :pospago), do: cadastrar(nome, numero, cpf, %Pospago{})
  def cadastrar(nome, numero, cpf, plano) do
    case buscar_assinante(numero) do
      nil ->
        assinante =
          %__MODULE__{nome: nome, numero: numero, cpf: cpf, plano: plano}

        (read(pega_plano(assinante.plano.__struct__)) ++ [assinante])
        |> :erlang.term_to_binary()
        |> write(pega_plano(assinante.plano.__struct__))

        {:ok, "Assinante #{nome} cadastrado com sucesso"}

      _assinant ->
        {:error, "Assinante com este número já é cadastrado!"}
    end
  end

  def pega_plano(Prepago), do: :prepago
  def pega_plano(Pospago), do: :pospago
  # defp pega_plano(assinante) do
  #   case assinante.plano.__struct__ == Prepago do
  #     true ->
  #       :prepago

  #     false ->
  #       :pospago
  #   end
  # end

  defp write(lista_assinantes, plano) do
    File.write!(@assinantes[plano], lista_assinantes)
  end

  def read(plano) do
    case File.read(@assinantes[plano]) do
      {:ok, assinantes} ->
        assinantes
        |> :erlang.binary_to_term()
        # |> IO.inspect(label: "Lendo Assinantes")
      {:error, :ennoent} ->
        {:error, "Arquivo invalido"}
    end
  end

  def deletar(numero) do
    {assinante, nova_lista} = deletar_item(numero)

    nova_lista
    |> :erlang.term_to_binary()
    |> write(pega_plano(assinante.plano.__struct__))

    {:ok, "Assinante #{assinante.nome} deletado!"}
  end

  def deletar_item(numero) do
    assinante =
      buscar_assinante(numero)

    nova_lista =
      read(pega_plano(assinante.plano.__struct__))
      |> List.delete(assinante)
      # |> IO.inspect(label: "Deletando Assinante")

    {assinante, nova_lista}
  end

  def atualizar(numero, assinante) do
    {assinante_antigo, nova_lista} = deletar_item(numero)

    case pega_plano(assinante.plano.__struct__) == pega_plano(assinante_antigo.plano.__struct__) do
      true ->
        (nova_lista ++ [assinante])
        |> :erlang.term_to_binary()
        |> write(pega_plano(assinante.plano.__struct__))

      false ->
        {:erro, "Assinante não pode alterar o plano"}
    end
  end

  @doc """
  Função para buscar assinante

  ## Parametros da função

  - numero: parametro do numero de busca do assinante.
  - key: parametro `:key` que tem o valor `:all` como default e os outros tipos são `[:prepago, :pospago]`

  ## Exemplo

      iex> Assinante.buscar_assinante("123")
      %Assinante{cpf: "123", nome: "Fulano", numero: "123", plano: %Prepago{}}
  """
  def buscar_assinante(numero, key \\ :all), do: buscar(numero, key)

  defp buscar(numero, :all), do: filtro(assinantes(), numero)
  defp buscar(numero, :prepago), do: filtro(assinantes_prepago(), numero)
  defp buscar(numero, :pospago), do: filtro(assinantes_postpago(), numero)

  defp filtro(lista, numero), do: Enum.find(lista, &(&1.numero == numero))

  @doc """
  Função que busca todos os assinantes

  ## Exemplo

      iex> Assinante.assinantes()
      [
        %Assinante{cpf: "123", nome: "Fulano", numero: "123", plano: %Prepago{}},
        %Assinante{cpf: "456", nome: "Sicrano", numero: "456", plano: %Prepago{}},
        %Assinante{cpf: "789", nome: "Beltrano", numero: "789", plano: %Pospago{}}
      ]
  """
  def assinantes(), do: read(:prepago) ++ read(:pospago)

  @doc """
  Função que busca todos os assinantes com o `plano: :prepago`

  ## Exemplo

      iex> Assinante.assinantes_prepago()
      [
       %Assinante{cpf: "123", nome: "Fulano", numero: "123", plano: %Prepago{}},
       %Assinante{cpf: "456", nome: "Sicrano", numero: "456", plano: %Prepago{}}
      ]
  """
  def assinantes_prepago(), do: read(:prepago)

  @doc """
  Função que busca todos os assinantes com o `plano: :pospago`

  ## Exemplo

      iex> Assinante.assinantes_postpago()
      [
       %Assinante{cpf: "789", nome: "Beltrano", numero: "789", plano: %Pospago{}}
      ]
  """
  def assinantes_postpago(), do: read(:pospago)
end
