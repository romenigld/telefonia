defmodule Telefonia.MixProject do
  use Mix.Project

  def project do
    [
      app: :telefonia,
      version: "0.1.0",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      #Docs
      name: "Telefonia",
      source_url: "https://gitlab.com/romenigld/telefonia.git",
      homepage_url: "http://telefonia-sample-elx-pro.com", # irá mudar para essa rota quando clicar no link Telefonia
      docs: [
        main: "Telefonia", # The main page in the docs
        # logo: "path/to/logo.png",
        extras: ["README.md"]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.24", only: :dev, runtime: false},
      {:earmark, "~> 1.3", only: [:dev]},
      {:mix_test_watch, "~> 1.0", only: :dev, runtime: false}
    ]
  end
end
