defmodule AssinanteTest do
  use ExUnit.Case
  doctest Assinante

  setup do
    File.write!("pre.txt", :erlang.term_to_binary([]))
    File.write!("pos.txt", :erlang.term_to_binary([]))

    Assinante.cadastrar("Fulano", "123", "123", :prepago)
    Assinante.cadastrar("Sicrano", "456", "456", :prepago)
    Assinante.cadastrar("Beltrano", "789", "789", :pospago)

    on_exit(fn ->
      File.rm("pre.txt")
      File.rm("pos.txt")
    end)

  end

  describe "responsaveis pelo cadastro de assinantes" do
    test "deve retornar estrutura de assinante" do
      assert %Assinante{nome: "Foo", numero: "321", cpf: "321", plano: :pospago}.nome == "Foo"
    end

    test "criar uma conta prepago" do
      assert Assinante.cadastrar("Foo", "012", "012", :prepago) ==
                {:ok, "Assinante Foo cadastrado com sucesso"}

      assert Assinante.buscar_assinante("012").plano.__struct__ == Prepago
    end

    test "deve retornar erro dizendo que assinante já esta cadastrado" do
      Assinante.cadastrar("Baz", "321", "321", :prepago)

      assert Assinante.cadastrar("Baz", "321", "321", :prepago) ==
                {:error, "Assinante com este número já é cadastrado!"}
    end
  end

  describe "responsaveis por busca de assinantes" do
    test "busca pospago" do
      assert Assinante.buscar_assinante("789", :pospago).nome == "Beltrano"
      assert Assinante.buscar_assinante("789", :pospago).plano.__struct__ == Pospago
    end


    test "busca prepago" do
      assert Assinante.buscar_assinante("456", :prepago).nome == "Sicrano"
      assert Assinante.buscar_assinante("456").plano.__struct__ == Prepago
    end
  end

  describe "delete" do
    test "deve deletear o assinante" do
      Assinante.cadastrar("Foo", "012", "012", :prepago)

      assert Assinante.deletar("012") ==
         {:ok, "Assinante Foo deletado!"}

      assert Assinante.buscar_assinante("012") == nil
    end
  end
end
