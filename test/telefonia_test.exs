defmodule TelefoniaTest do
  use ExUnit.Case
  doctest Telefonia

  setup do
    assert Telefonia.start() == :ok

    {:ok, files} = File.ls
    assert Enum.any?(files, fn x -> x == "pre.txt" end) == true
    assert Enum.any?(files, &(&1== "pos.txt")) == true

    Assinante.cadastrar("Romenig", "246", "246", :prepago)
    Assinante.cadastrar("Minho", "357", "357", :pospago)

    on_exit(fn ->
      File.rm("pre.txt")
      File.rm("pos.txt")
    end)

  end

  describe "responsaveis pelo cadastro de assinantes" do
     test "criar uma conta prepago" do
      assert Telefonia.cadastrar_assinante("Foo", "012", "012", :prepago) ==
                {:ok, "Assinante Foo cadastrado com sucesso"}

      assert Telefonia.buscar_por_numero("012").plano.__struct__ == Prepago
    end

    test "criar uma conta pospago" do
      assert Telefonia.cadastrar_assinante("Baz", "0987", "0987", :pospago) ==
                {:ok, "Assinante Baz cadastrado com sucesso"}

      assert Telefonia.buscar_por_numero("0987").plano.__struct__ == Pospago
    end

    test "deve retornar erro dizendo que assinante já esta cadastrado" do
      Telefonia.cadastrar_assinante("Foo", "012", "012", :prepago)

      assert Telefonia.cadastrar_assinante("Baz", "012", "012", :prepago) ==
                {:error, "Assinante com este número já é cadastrado!"}
    end
  end

  describe "responsaveis por listar assinantes" do
    test "busca todos" do
      assert Enum.count(Telefonia.listar_assinantes()) == 2
    end

    test "busca prepago" do
      assert Enum.count(Telefonia.listar_assinantes_prepago()) == 1
    end

    test "busca pospago" do
      assert Enum.count(Telefonia.listar_assinantes_pospago()) == 1
    end
  end

  describe "realizar chamadas" do
    test "deve retornar erro quando não for realizado uma recarga" do
     assert Telefonia.fazer_chamada("246", :prepago, DateTime.utc_now, 10) ==
        {:error, "Você não tem créditos para fazer a ligação, faça uma recarga"}
    end

    test "chamada Prepago realizada com sucesso" do
     assert Telefonia.recarga("246", DateTime.utc_now(), 100) ==
              {:ok, "Recarga realizada com sucesso!"}

      assert Telefonia.fazer_chamada("246", :prepago, DateTime.utc_now, 10) ==
              {:ok, "A chamada custou 14.5, e você tem 85.5 de créditos"}
    end

    # test "chamada Pospago realizada com sucesso" do
    #   assert Telefonia.recarga("357", DateTime.utc_now(), 100) ==
    #            {:ok, "Recarga realizada com sucesso!"}

    #   assert Telefonia.fazer_chamada("357", :pospago, DateTime.utc_now, 10) ==
    #            {:ok, "A chamada custou 14.5, e você tem 85.5 de créditos"}
    #  end
  end

  test "imprimir contas" do
    data =
      ~U[2021-06-14 16:10:56.040235Z]

    assert Telefonia.recarga("246", data, 100) ==
                {:ok, "Recarga realizada com sucesso!"}

    assert Telefonia.fazer_chamada("246", :prepago, data, 10) ==
                {:ok, "A chamada custou 14.5, e você tem 85.5 de créditos"}

    assert Telefonia.imprimir_contas(06, 2021) == :ok
    # Info da impressão que retorna
    # """
    # Conta do tipo Prepaga do Assinante: Romenig
    # Numero: 246
    # Chamadas: [%Chamada{data: #{data}, duracao: 10}]
    # Recargas: [%Recarga{data: #{data}, valor: 100}]
    # Total de Chamadas: 1
    # Total de Recargas: 1
    # ===========================================================
    # Conta do tipo Pospago do Assinante: Minho
    # Numero: 357
    # Chamadas: []
    # Total de Chamadas: 0
    # Valor da Fatura: 0
    # ===========================================================
    # """
  end
end
