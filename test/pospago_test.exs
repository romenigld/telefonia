defmodule PospagoTest do
  use ExUnit.Case

  setup do
    File.write!("pre.txt", :erlang.term_to_binary([]))
    File.write!("pos.txt", :erlang.term_to_binary([]))

    on_exit(fn ->
      File.rm("pre.txt")
      File.rm("pos.txt")
    end)
  end

  test "deve fazer uma ligação" do
    Assinante.cadastrar("Baz", "012", "012", :pospago)

    assert Pospago.fazer_chamada("012", DateTime.utc_now(), 5) ==
              {:ok, "Chamada feita com sucesso! duração: 5 minutos"}

  end

  test "deve imprimir a conta do assinante" do
    Assinante.cadastrar("Baz", "012", "012", :pospago)
    data =
      DateTime.utc_now()
    data_antiga =
      ~U[2021-05-09 16:01:34.441508Z]

    Pospago.fazer_chamada("012", data, 3)
    Pospago.fazer_chamada("012", data_antiga, 3)
    Pospago.fazer_chamada("012", data, 3)
    Pospago.fazer_chamada("012", data, 3)

    assinante =
      Assinante.buscar_assinante("012", :pospago)

    assert Enum.count(assinante.chamadas) == 4

    assinante =
      Pospago.imprimir_conta(data.month, data.year, "012")

    assert assinante.numero == "012"
    assert Enum.count(assinante.chamadas) == 3
    assert assinante.plano.valor == 12.599999999999998
  end

end
