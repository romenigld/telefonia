defmodule PrepagoTest do
  use ExUnit.Case
  doctest Prepago

  setup do
    File.write!("pre.txt", :erlang.term_to_binary([]))
    File.write!("pos.txt", :erlang.term_to_binary([]))

    # Assinante.cadastrar("Fulano", "123", "123")
    # Assinante.cadastrar("Sicrano", "456", "456", :prepago)
    # Assinante.cadastrar("Beltrano", "789", "789", :pospago)

    on_exit(fn ->
      File.rm("pre.txt")
      File.rm("pos.txt")
    end)
  end

  describe "Funções de ligação" do
    test "fazer uma ligação" do
      Assinante.cadastrar("Baz", "012", "012", :prepago)
      data = DateTime.utc_now()

      Recarga.nova(data, 10, "012")

      assert Prepago.fazer_chamada("012", data, 3) ==
        {:ok,  "A chamada custou 4.35, e você tem 5.65 de créditos"}
    end

    test "fazer uma ligação longa e não tem créditos" do
      Assinante.cadastrar("Baz", "012", "012", :prepago)

      assert Prepago.fazer_chamada("012", DateTime.utc_now(), 10) ==
        {:error, "Você não tem créditos para fazer a ligação, faça uma recarga"}
    end
  end

  describe "para impressão de contas" do
    test "deve informar valores de contas do mês" do
      Assinante.cadastrar("Baz", "012", "012", :prepago)
      data =
        DateTime.utc_now()
      data_antiga =
        ~U[2021-05-09 16:01:34.441508Z]
      Recarga.nova(data, 10, "012")
      Prepago.fazer_chamada("012", data, 3)
      Recarga.nova(data_antiga, 10, "012")
      Prepago.fazer_chamada("012", data_antiga, 3)

      assinante =
        Assinante.buscar_assinante("012", :prepago)

      assert Enum.count(assinante.chamadas) == 2
      assert Enum.count(assinante.plano.recargas) == 2

      assinante =
        Prepago.imprimir_conta(data.month, data.year, "012")

      assert assinante.numero == "012"
      assert Enum.count(assinante.chamadas) == 1
      assert Enum.count(assinante.plano.recargas) == 1

    end
  end
end
